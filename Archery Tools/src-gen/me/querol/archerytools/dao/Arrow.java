package me.querol.archerytools.dao;

import de.greenrobot.dao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table ARROW.
 */
public class Arrow {

    private Long id;
    private Integer arrowNumber;
    private String arrowImage;
    private String arrowText;
    private String textColor;
    private Integer arrowValue;
    private Integer arrowOrder;
    private long parentId;

    /** Used to resolve relations */
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    private transient ArrowDao myDao;

    private End parent;
    private Long parent__resolvedKey;


    public Arrow() {
    }

    public Arrow(Long id) {
        this.id = id;
    }

    public Arrow(Long id, Integer arrowNumber, String arrowImage, String arrowText, String textColor, Integer arrowValue, Integer arrowOrder, long parentId) {
        this.id = id;
        this.arrowNumber = arrowNumber;
        this.arrowImage = arrowImage;
        this.arrowText = arrowText;
        this.textColor = textColor;
        this.arrowValue = arrowValue;
        this.arrowOrder = arrowOrder;
        this.parentId = parentId;
    }

    /** called by internal mechanisms, do not call yourself. */
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getArrowDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getArrowNumber() {
        return arrowNumber;
    }

    public void setArrowNumber(Integer arrowNumber) {
        this.arrowNumber = arrowNumber;
    }

    public String getArrowImage() {
        return arrowImage;
    }

    public void setArrowImage(String arrowImage) {
        this.arrowImage = arrowImage;
    }

    public String getArrowText() {
        return arrowText;
    }

    public void setArrowText(String arrowText) {
        this.arrowText = arrowText;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public Integer getArrowValue() {
        return arrowValue;
    }

    public void setArrowValue(Integer arrowValue) {
        this.arrowValue = arrowValue;
    }

    public Integer getArrowOrder() {
        return arrowOrder;
    }

    public void setArrowOrder(Integer arrowOrder) {
        this.arrowOrder = arrowOrder;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    /** To-one relationship, resolved on first access. */
    public End getParent() {
        long __key = this.parentId;
        if (parent__resolvedKey == null || !parent__resolvedKey.equals(__key)) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            EndDao targetDao = daoSession.getEndDao();
            End parentNew = targetDao.load(__key);
            synchronized (this) {
                parent = parentNew;
            	parent__resolvedKey = __key;
            }
        }
        return parent;
    }

    public void setParent(End parent) {
        if (parent == null) {
            throw new DaoException("To-one property 'parentId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.parent = parent;
            parentId = parent.getId();
            parent__resolvedKey = parentId;
        }
    }

    /** Convenient call for {@link AbstractDao#delete(Object)}. Entity must attached to an entity context. */
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.delete(this);
    }

    /** Convenient call for {@link AbstractDao#update(Object)}. Entity must attached to an entity context. */
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.update(this);
    }

    /** Convenient call for {@link AbstractDao#refresh(Object)}. Entity must attached to an entity context. */
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.refresh(this);
    }

}
