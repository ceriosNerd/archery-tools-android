package me.querol.archerytools;

/**
 * Created by winsock on 5/8/14.
 */
public class RoundDOM {
    private String distance;
    private int arrowsPerEnd, endsPerRound;

    public int getArrowsPerEnd() {
        return arrowsPerEnd;
    }

    public void setArrowsPerEnd(int arrowsPerEnd) {
        this.arrowsPerEnd = arrowsPerEnd;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getEndsPerRound() {
        return endsPerRound;
    }

    public void setEndsPerRound(int endsPerRound) {
        this.endsPerRound = endsPerRound;
    }
}
