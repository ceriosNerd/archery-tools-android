package me.querol.archerytools;

import android.app.Application;
import android.content.Context;
import android.util.TypedValue;

import java.util.Date;

import me.querol.archerytools.dao.Arrow;
import me.querol.archerytools.dao.ArrowDao;
import me.querol.archerytools.dao.DaoMaster;
import me.querol.archerytools.dao.DaoSession;
import me.querol.archerytools.dao.End;
import me.querol.archerytools.dao.EndDao;
import me.querol.archerytools.dao.Event;
import me.querol.archerytools.dao.EventDao;
import me.querol.archerytools.dao.Round;
import me.querol.archerytools.dao.RoundDao;
import me.querol.archerytools.dao.ScoreCard;
import me.querol.archerytools.dao.ScoreCardDao;

/**
 * Created by winsock on 6/4/14.
 */
public class ArcheryToolsApp extends Application {

    private static Context context;
    private static DaoSession daoSingleton;

    public void onCreate() {
        context = getApplicationContext();
    }

    public static DaoSession getSessionSingleton() {
        if (daoSingleton == null || !daoSingleton.getDatabase().isOpen()) {
            daoSingleton = new DaoMaster(new DaoMaster.DevOpenHelper(ArcheryToolsApp.getContext(), "archery-db", null).getWritableDatabase()).newSession();
        }
        return daoSingleton;
    }

    public static Context getContext() {
        return context;
    }

    public static ScoreCard generateScoreCard(EventDOM eventDOM, String archerName, String eventName, Date date) {
        DaoSession session = ArcheryToolsApp.getSessionSingleton();

        ScoreCardDao scoreCardDao = session.getScoreCardDao();
        ArrowDao arrowDao = session.getArrowDao();
        EndDao endDao = session.getEndDao();
        RoundDao roundDao = session.getRoundDao();
        EventDao eventDao = session.getEventDao();

        ScoreCard scoreCard = new ScoreCard();
        scoreCard.setArcherName(archerName);
        scoreCard.setEventDate(date);
        scoreCard.setEventName(eventName);
        scoreCardDao.insert(scoreCard);

        Event event = new Event(null, eventDOM.getKeyboardFile(), eventDOM.getName(), scoreCard.getId(), null);
        eventDao.insert(event);
        scoreCard.setEvent(event);
        event.setParent(scoreCard);
        event.update();
        scoreCard.update();

        for (int r = 0; r < eventDOM.getRounds().size(); r++) {
            RoundDOM roundDOM = eventDOM.getRounds().get(r);
            Round round = new Round(null, r + 1, roundDOM.getDistance(), roundDOM.getArrowsPerEnd(), roundDOM.getEndsPerRound(), 0, 0, 0f, 0, event.getId(), null);
            roundDao.insert(round);
            for (int e = 0; e < roundDOM.getEndsPerRound(); e++) {
                End end = new End(null, e + 1, 0, 0, 0, 0, new byte[0], round.getId(), null);
                endDao.insert(end);
                for (int a = 0; a < roundDOM.getArrowsPerEnd(); a++) {
                    Arrow arrow = new Arrow(null, a + 1, "gray", "", "black", 0, -2, end.getId());
                    end.getArrows().add(arrow);
                    arrowDao.insert(arrow);
                }
                round.getEnds().add(end);
                end.update();
            }
            event.getRounds().add(round);
            round.update();
        }
        event.update();
        return scoreCard;
    }

    public static int getDisplayPixelSize(int dps) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dps, getContext().getResources().getDisplayMetrics());
    }

    public static int getDrawable(String pDrawableName, String packageName) {
        return context.getResources().getIdentifier(pDrawableName, "drawable", packageName);
    }
}
