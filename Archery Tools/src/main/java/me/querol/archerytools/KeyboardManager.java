package me.querol.archerytools;

import android.app.ProgressDialog;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Xml;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import org.xmlpull.v1.XmlPullParser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by winsock on 6/19/14.
 */
public class KeyboardManager {
    private PopupWindow keyboardPopup = null;
    private final View parent;
    private final List<List<KeyDOM>> xmlKeyDOMList;
    private final List<SpecialKeyDOM> xmlSpecialKeyDOMList;
    private KeyPress keyPressListener;

    public interface KeyPress {
        void onScoringKeyPress(KeyDOM key);

        void onSpecialKeyPress(SpecialKeyDOM specialKey);
    }

    public KeyboardManager(View parent, String keyboardFile) {
        this.parent = parent;
        xmlKeyDOMList = new ArrayList<>();
        xmlSpecialKeyDOMList = new ArrayList<>();
        new AsyncUpdaterThread().execute(keyboardFile);
    }

    public void setOnKeyPressListener(KeyPress keyPressListener) {
        this.keyPressListener = keyPressListener;
    }

    private class AsyncUpdaterThread extends AsyncTask<String, Void, String> {
        private ProgressDialog progress = null;

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(parent.getContext(), "Downloading Keyboard", "Please wait...");
        }

        @Override
        protected String doInBackground(String... strings) {
            if (strings == null)
                return null;

            String xmlFile = strings[0];

            // XXX: XML Keyboard Files

            return xmlFile;
        }

        @Override
        protected void onPostExecute(String keyboardFile) {
            XmlPullParser xmlPullParser = Xml.newPullParser();
            InputStream xmlFile = null;
            try {
                xmlFile = parent.getContext().openFileInput(keyboardFile);
                xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                xmlPullParser.setInput(xmlFile, null);
                int eventType = xmlPullParser.next();

                List<KeyDOM> row = new ArrayList<>();

                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if (eventType == XmlPullParser.START_TAG) {
                        if (xmlPullParser.getName().equalsIgnoreCase("key")) {
                            row.add(new KeyDOM(xmlPullParser.getAttributeValue(0), Integer.parseInt(xmlPullParser.getAttributeValue(1)), Integer.parseInt(xmlPullParser.getAttributeValue(2)), xmlPullParser.getAttributeValue(3), xmlPullParser.getAttributeValue(4)));
                        } else if (xmlPullParser.getName().equalsIgnoreCase("specialKey")) {
                            xmlSpecialKeyDOMList.add(new SpecialKeyDOM(Integer.parseInt(xmlPullParser.getAttributeValue(0)), Integer.parseInt(xmlPullParser.getAttributeValue(1)), Integer.parseInt(xmlPullParser.getAttributeValue(2)), xmlPullParser.getAttributeValue(3)));
                        }
                    } else if (eventType == XmlPullParser.END_TAG) {
                        if (xmlPullParser.getName().equalsIgnoreCase("row") && row.size() > 0) {
                            xmlKeyDOMList.add(row);
                            row = new ArrayList<>();
                        }
                    }
                    eventType = xmlPullParser.next();
                }
            } catch (Exception e) {
                Logger.getLogger("Archery Tools").log(Level.SEVERE, e.getLocalizedMessage());
            }

            if (xmlFile != null) {
                try {
                    xmlFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            progress.dismiss();
        }
    }

    public void show() {
        if (keyboardPopup == null) {
            final LinearLayout keyboardView = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.keyboard, null, false);
            // Scoring Keys
            for (List<KeyDOM> row : xmlKeyDOMList) {
                LinearLayout viewRow = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.keyboard_row, keyboardView, false);
                for (KeyDOM key : row) {
                    View scoreButtonCell = LayoutInflater.from(parent.getContext()).inflate(R.layout.arrow_cell, viewRow, false);
                    Button button = (Button) scoreButtonCell.findViewById(R.id.arrow_button);
                    button.setText(key.getKeyText());
                    button.setBackgroundResource(ArcheryToolsApp.getDrawable(key.getKeyColorImage(), parent.getContext().getPackageName()));
                    button.setTextColor(ColorStateList.valueOf(Color.parseColor(key.getTextColor())));
                    button.setTag(key);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            keyPressed((KeyDOM) view.getTag());
                        }
                    });
                    viewRow.addView(scoreButtonCell);
                }
                keyboardView.addView(viewRow);
            }

            // Special Keys
            LinearLayout specialKeyRow = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.keyboard_row, keyboardView, false);
            for (SpecialKeyDOM specialKey : xmlSpecialKeyDOMList) {
                View scoreButtonCell = LayoutInflater.from(parent.getContext()).inflate(R.layout.arrow_cell, specialKeyRow, false);
                Button button = (Button) scoreButtonCell.findViewById(R.id.arrow_button);
                button.setLayoutParams(new RelativeLayout.LayoutParams(ArcheryToolsApp.getDisplayPixelSize(specialKey.getWidth()), ArcheryToolsApp.getDisplayPixelSize(specialKey.getHeight())));
                switch (specialKey.getCode()) {
                    case 3:
                        button.setText(R.string.keyboard_dismiss_text);
                        break;
                    case 1:
                    case 2:
                    default:
                        break;
                }
                if (specialKey.getImage() != null && !specialKey.getImage().isEmpty() && !specialKey.getImage().equalsIgnoreCase("n/a")) {
                    if (specialKey.getCode() == 1) {
                        button.setBackgroundResource(ArcheryToolsApp.getDrawable("bksp", parent.getContext().getPackageName()));
                    } else {
                        button.setBackgroundResource(ArcheryToolsApp.getDrawable(specialKey.getImage(), parent.getContext().getPackageName()));
                    }
                }
                button.setTag(specialKey);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        specialPressed((SpecialKeyDOM) view.getTag());
                    }
                });
                specialKeyRow.addView(scoreButtonCell);
            }

            keyboardView.addView(specialKeyRow);
            keyboardPopup = new PopupWindow(keyboardView, parent.getMeasuredWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        keyboardPopup.showAtLocation(parent, Gravity.BOTTOM | Gravity.CENTER, 0, 0);
    }

    public void hide() {
        if (keyboardPopup.isShowing())
            keyboardPopup.dismiss();
    }

    public void keyPressed(KeyDOM key) {
        keyPressListener.onScoringKeyPress(key);
    }

    public void specialPressed(SpecialKeyDOM specialKey) {
        keyPressListener.onSpecialKeyPress(specialKey);
    }
}
