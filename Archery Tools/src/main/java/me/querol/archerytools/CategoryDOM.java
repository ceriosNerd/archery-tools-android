package me.querol.archerytools;

import java.util.ArrayList;

/**
 * Created by winsock on 5/8/14.
 */
public class CategoryDOM {
    private String name;
    private ArrayList<EventDOM> events = new ArrayList<EventDOM>();

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<EventDOM> getEvents() {
        return events;
    }

    public void addEvent(EventDOM event) {
        events.add(event);
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
