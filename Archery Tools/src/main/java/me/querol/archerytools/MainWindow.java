package me.querol.archerytools;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.DataSetObserver;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Xml;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import me.querol.archerytools.dao.ScoreCard;

public class MainWindow extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_window);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {

        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, new FragmentMainWindow()).commit();
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setTitle(mTitle);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main_window, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    /**
     * The Main Fragment
     */
    public static class FragmentMainWindow extends Fragment {
        private ArrayList<CategoryDOM> categories = new ArrayList<>();

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main_window, container, false);
            new AsyncUpdaterThread(rootView.getContext()).execute();
            return rootView;
        }

        private class AsyncUpdaterThread extends AsyncTask<Void, Void, Void> {

            private final Context mContext;
            private ProgressDialog progress = null;

            private final ExpandableListAdapter adapter = new ExpandableListAdapter() {

                @Override
                public void registerDataSetObserver(DataSetObserver dataSetObserver) {
                }

                @Override
                public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
                }

                @Override
                public int getGroupCount() {
                    return categories.size();
                }

                @Override
                public int getChildrenCount(int i) {
                    return categories.get(i).getEvents().size();
                }

                @Override
                public Object getGroup(int i) {
                    return categories.get(i).getEvents();
                }

                @Override
                public Object getChild(int i, int i2) {
                    return categories.get(i).getEvents().get(i2);
                }

                @Override
                public long getGroupId(int i) {
                    return categories.get(i).hashCode();
                }

                @Override
                public long getChildId(int i, int i2) {
                    return categories.get(i).getEvents().get(i2).hashCode();
                }

                @Override
                public boolean hasStableIds() {
                    return true;
                }

                @Override
                public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
                    TextView textView;
                    if (view == null || !TextView.class.isInstance(view)) {
                        textView = new TextView(mContext);
                    } else {
                        textView = (TextView) view;
                    }
                    textView.setHeight(70);
                    textView.setText(categories.get(i).toString());
                    return textView;
                }

                @Override
                public View getChildView(int i, int i2, boolean b, View view, ViewGroup viewGroup) {
                    TextView textView;
                    if (view == null || !TextView.class.isInstance(view)) {
                        textView = new TextView(mContext);
                    } else {
                        textView = (TextView) view;
                    }
                    textView.setHeight(60);
                    textView.setText(categories.get(i).getEvents().get(i2).toString());
                    return textView;
                }

                @Override
                public boolean isChildSelectable(int i, int i2) {
                    return true;
                }

                @Override
                public boolean areAllItemsEnabled() {
                    return true;
                }

                @Override
                public boolean isEmpty() {
                    return categories.size() <= 0;
                }

                @Override
                public void onGroupExpanded(int i) {
                }

                @Override
                public void onGroupCollapsed(int i) {

                }

                @Override
                public long getCombinedChildId(long l, long l2) {
                    return l + l2;
                }

                @Override
                public long getCombinedGroupId(long l) {
                    return l;
                }
            };

            private AsyncUpdaterThread(Context mContext) {
                this.mContext = mContext;
            }

            @Override
            protected void onPreExecute() {
                progress = ProgressDialog.show(mContext, "Checking for new events", "Please wait...");
            }

            @Override
            protected Void doInBackground(Void... nothing) {
                // preset_rounds.xml
                return null;
            }

            @Override
            protected void onPostExecute(Void nothing) {
                ExpandableListView selectionListView = (ExpandableListView) getActivity().findViewById(R.id.roundList);

                categories = new EventXMLParser(mContext, "preset_rounds.xml").parse();


                selectionListView.setAdapter(adapter);
                selectionListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView expandableListView, View view, final int i, final int i2, long l) {
                        final View content = View.inflate(mContext, R.layout.new_scorecard_popup, null);
                        final PopupWindow window = new PopupWindow(mContext);
                        final FragmentManager manager = getFragmentManager();
                        content.findViewById(R.id.quick_start).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                FragmentTransaction trans = manager.beginTransaction();

                                ScoreCard scoreCard = ArcheryToolsApp.generateScoreCard(categories.get(i).getEvents().get(i2), "Owner", "Practice", new Date());

                                // Set up fragment
                                ScoringFragment scoringFragment = new ScoringFragment();
                                scoringFragment.setScoreCard(scoreCard);

                                trans.replace(R.id.container, scoringFragment, "fragment_scoring");
                                trans.commit();
                                window.dismiss();
                            }
                        });
                        content.findViewById(R.id.new_card).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                content.findViewById(R.id.new_card).setVisibility(View.GONE);
                                content.findViewById(R.id.quick_start).setVisibility(View.GONE);

                                content.findViewById(R.id.start_event).setVisibility(View.VISIBLE);
                                content.findViewById(R.id.event_name).setVisibility(View.VISIBLE);
                                content.findViewById(R.id.event_name_label).setVisibility(View.VISIBLE);
                                content.findViewById(R.id.archer_name).setVisibility(View.VISIBLE);
                                content.findViewById(R.id.date).setVisibility(View.VISIBLE);
                            }
                        });
                        content.findViewById(R.id.start_event).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                String eventName = ((EditText) content.findViewById(R.id.event_name)).getText().toString();
                                String archerName = ((EditText) content.findViewById(R.id.archer_name)).getText().toString();

                                FragmentTransaction trans = manager.beginTransaction();

                                ScoreCard scoreCard = ArcheryToolsApp.generateScoreCard(categories.get(i).getEvents().get(i2), archerName, eventName, getDateFromDatePicker((DatePicker) content.findViewById(R.id.date)));

                                // Set up fragment
                                ScoringFragment scoringFragment = new ScoringFragment();
                                scoringFragment.setScoreCard(scoreCard);

                                trans.replace(R.id.container, scoringFragment, "fragment_scoring");
                                trans.commit();
                                window.dismiss();
                            }
                        });
                        window.setContentView(content);
                        window.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                        window.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                        window.setFocusable(true);
                        window.showAtLocation(getView(), Gravity.CENTER, 0, 0);
                        return true;
                    }
                });
                progress.dismiss();
            }
        }
    }

    public static java.util.Date getDateFromDatePicker(DatePicker datePicker) {
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        //noinspection ResourceType
        calendar.set(year, month, day);

        return calendar.getTime();
    }

    private static class EventXMLParser {
        private XmlPullParser xmlPullParser = Xml.newPullParser();

        private CategoryDOM categoryInProgress = null;
        private EventDOM eventInWork = null;
        private RoundDOM roundInWork = null;
        private String tagData = "";
        private String mainEventName = "";
        private String currentKeyboardFile = "";
        private InputStream xmlFile = null;

        private ArrayList<CategoryDOM> categories = new ArrayList<>();

        public EventXMLParser(Context mContext, String file) {
            try {
                xmlFile = mContext.openFileInput(file);
            } catch (FileNotFoundException e) {
                Log.wtf("Archery Tools", e);
            }
        }

        public ArrayList<CategoryDOM> parse() {
            if (xmlFile == null) {
                return new ArrayList<>();
            }

            try {
                xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                xmlPullParser.setInput(xmlFile, null);

                int eventType = xmlPullParser.next();
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if (eventType == XmlPullParser.START_TAG) {
                        processStartTag();
                    } else if (eventType == XmlPullParser.END_TAG) {
                        processEndTag();
                    } else if (eventType == XmlPullParser.TEXT) {
                        if (!xmlPullParser.getText().trim().isEmpty())
                            tagData = xmlPullParser.getText();
                    }
                    eventType = xmlPullParser.next();
                }
            } catch (Exception ignored) {
                return new ArrayList<>();
            } finally {
                if (xmlFile != null) {
                    try {
                        xmlFile.close();
                    } catch (IOException e) { /* I Give Up */ }
                }
            }

            return categories;
        }

        private void processStartTag() {
            if (xmlPullParser.getName().equalsIgnoreCase("category")) {
                categoryInProgress = new CategoryDOM();
                categoryInProgress.setName(xmlPullParser.getAttributeValue(0));
            } else if (xmlPullParser.getName().equalsIgnoreCase("round")) {
                roundInWork = new RoundDOM();
            } else if (xmlPullParser.getName().equalsIgnoreCase("event")) {
                eventInWork = new EventDOM();
                eventInWork.setName(xmlPullParser.getAttributeValue(0));
            } else if (xmlPullParser.getName().equalsIgnoreCase("subtype")) {
                if (eventInWork == null) {
                    eventInWork = new EventDOM();
                    eventInWork.setName(xmlPullParser.getAttributeValue(0) + " " + mainEventName);
                    eventInWork.setKeyboardFile(currentKeyboardFile);
                } else {
                    mainEventName = eventInWork.getName();
                    currentKeyboardFile = eventInWork.getKeyboardFile();
                }
            }
        }

        private void processEndTag() {
            switch (xmlPullParser.getName()) {
                case "keyboard":
                    eventInWork.setKeyboardFile(tagData);
                    break;
                case "distance":
                    roundInWork.setDistance(tagData);
                    break;
                case "arrowsPerEnd":
                    roundInWork.setArrowsPerEnd(Integer.parseInt(tagData));
                    break;
                case "endsPerRound":
                    roundInWork.setEndsPerRound(Integer.parseInt(tagData));
                    break;
                case "subtype":
                    categoryInProgress.addEvent(eventInWork);
                    break;
                case "round":
                    eventInWork.addRound(roundInWork);
                    roundInWork = null;
                    break;
                case "event":
                    categoryInProgress.addEvent(eventInWork);
                    eventInWork = null;
                    break;
                case "category":
                    categories.add(categoryInProgress);
                    categoryInProgress = null;
                    break;
            }
        }
    }
}
