package me.querol.archerytools;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;

import me.querol.archerytools.dao.Arrow;
import me.querol.archerytools.dao.End;
import me.querol.archerytools.dao.Round;
import me.querol.archerytools.dao.ScoreCard;

/**
 * Created by Andrew Querol on 6/4/14.
 * <p/>
 * Copyright 2014 Andrew Querol
 */
public class RoundAdapter extends BaseAdapter {

    private final ScoreCard scoreCard;
    private final KeyboardManager keyboardManager;
    private final Context context;
    private LayoutInflater inflater;
    private Button selectedArrow = null;

    private class ArrowInfo {
        public final Arrow arrowObject;
        public Button nextButton;

        public ArrowInfo(Arrow arrow) {
            this.arrowObject = arrow;
        }

        public void setNextButton(Button button) {
            this.nextButton = button;
        }
    }

    public RoundAdapter(final Context context, final ScoreCard scoreCard, final KeyboardManager manager) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.scoreCard = scoreCard;
        this.keyboardManager = manager;
        keyboardManager.setOnKeyPressListener(new KeyboardManager.KeyPress() {
            @Override
            public void onScoringKeyPress(KeyDOM key) {
                ArrowInfo arrowInfo = (ArrowInfo) selectedArrow.getTag();
                arrowInfo.arrowObject.setArrowValue(key.getKeyValue());
                arrowInfo.arrowObject.setArrowText(key.getKeyText());
                arrowInfo.arrowObject.setArrowImage(key.getKeyColorImage());
                arrowInfo.arrowObject.setTextColor(key.getTextColor());
                arrowInfo.arrowObject.setArrowOrder(key.getKeyRank());
                arrowInfo.arrowObject.update();
                updateArrowButton(selectedArrow);

                if (arrowInfo.nextButton != null) {
                    selectedArrow = arrowInfo.nextButton;
                    selectedArrow.setText("?");
                    selectedArrow.setBackgroundResource(ArcheryToolsApp.getDrawable("green", context.getPackageName()));
                } else {
                    sortEnd(arrowInfo.arrowObject.getParent());
                    selectedArrow = null;
                    keyboardManager.hide();
                }
            }

            @Override
            public void onSpecialKeyPress(SpecialKeyDOM specialKey) {
                switch (specialKey.getCode()) {
                    case 3:
                        if (selectedArrow != null) {
                            updateArrowButton(selectedArrow);
                            sortEnd(((ArrowInfo) selectedArrow.getTag()).arrowObject.getParent());
                        }
                        selectedArrow = null;
                        keyboardManager.hide();
                        break;
                    default:
                        break;
                }
            }
        });
    }

    public void updateArrowButton(Button button) {
        ArrowInfo arrowInfo = (ArrowInfo) button.getTag();
        button.setText(arrowInfo.arrowObject.getArrowText());
        button.setTextColor(Color.parseColor(arrowInfo.arrowObject.getTextColor()));
        button.setBackgroundResource(ArcheryToolsApp.getDrawable(arrowInfo.arrowObject.getArrowImage(), context.getPackageName()));
    }

    public void sortEnd(End end) {
        Collections.sort(end.getArrows(), new Comparator<Arrow>() {
            @Override
            public int compare(Arrow arrow, Arrow arrow2) {
                if (arrow.getArrowOrder() < 0 && arrow2.getArrowOrder() < 0)
                    return arrow.getArrowOrder() - arrow2.getArrowOrder();
                if (arrow.getArrowOrder() > 0 && arrow2.getArrowOrder() > 0)
                    return arrow2.getArrowOrder() - arrow.getArrowOrder();
                if (arrow2.getArrowOrder() < 0 && arrow.getArrowOrder() > 0)
                    return 1;
                if (arrow.getArrowOrder() < 0 && arrow2.getArrowOrder() > 0)
                    return -1;
                return 0; // Both arrow values are equal
            }
        });

        // Update the arrow numbers
        for (int i = 0; i < end.getArrows().size(); i++) {
            Arrow a = end.getArrows().get(i);
            a.setArrowNumber(i + 1);
            a.update();
        }

        // Save our sorting
        end.update();
    }

    @Override
    public int getCount() {
        return scoreCard.getEvent().getRounds().size();
    }

    @Override
    public Object getItem(int i) {
        return scoreCard.getEvent().getRounds().get(i);
    }

    @Override
    public long getItemId(int i) {
        return scoreCard.getEvent().getRounds().get(i).getId();
    }

    @Override
    public View getView(int i, View reuseView, ViewGroup viewGroup) {
        if (reuseView == null) {
            reuseView = inflater.inflate(R.layout.round_row, viewGroup, false);
        }

        Round roundForRow = scoreCard.getEvent().getRounds().get(i);
        LinearLayout endLayout = (LinearLayout) reuseView.findViewById(R.id.round_cell);
        for (End e : roundForRow.getEnds()) {
            RelativeLayout endView;
            if (endLayout.getChildCount() < e.getEndNumber()) {
                endView = (RelativeLayout) inflater.inflate(R.layout.end_row, endLayout, false);
                endView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                endLayout.addView(endView);
            } else {
                endView = (RelativeLayout) endLayout.getChildAt(e.getEndNumber() - 1);
            }

            TextView endNumberText = (TextView) endView.findViewById(R.id.end_number_text);
            TextView endTotalText = (TextView) endView.findViewById(R.id.end_total_text);
            TextView runningTotalText = (TextView) endView.findViewById(R.id.running_total_text);
            endNumberText.setText(e.getEndNumber().toString());
            endTotalText.setText(e.getEndTotal().toString());
            runningTotalText.setText(e.getRunningTotal().toString());
            LinearLayout arrowLayout = (LinearLayout) endView.findViewById(R.id.arrow_rows);

            // Number of arrows configured for this end
            int arrowsConfigured = 0;
            // Check for existing arrows to reuse
            if (arrowLayout.getChildCount() > 0) {
                // Prune any extra rows.
                while (arrowLayout.getChildCount() * 6 / e.getArrows().size() > 1 && arrowLayout.getChildCount() * 6 % e.getArrows().size() > 6) {
                    arrowLayout.removeViewAt(arrowLayout.getChildCount() - 1); // Change to zero based index
                }

                // Configure the existing rows
                for (int arrowRowNumber = 0; arrowRowNumber < arrowLayout.getChildCount(); arrowRowNumber++) {
                    LinearLayout arrowRow = (LinearLayout) arrowLayout.getChildAt(arrowRowNumber);

                    // Prune any extra arrows from the row if need be and add any extra arrows if needed
                    if (e.getArrows().size() - arrowsConfigured < 6) {
                        if (e.getArrows().size() - arrowsConfigured > arrowRow.getChildCount()) {
                            while (e.getArrows().size() - arrowsConfigured > arrowRow.getChildCount()) {
                                View arrowCell = inflater.inflate(R.layout.arrow_cell, arrowLayout, false);
                                arrowRow.addView(arrowCell);
                            }
                        } else if (arrowRow.getChildCount() > e.getArrows().size() - arrowsConfigured) {
                            while (arrowRow.getChildCount() > e.getArrows().size() - arrowsConfigured) {
                                arrowRow.removeViewAt(arrowRow.getChildCount() - 1);
                            }
                        }
                    }

                    Button prevButton = null;
                    for (int arrowViewNumber = 0; arrowViewNumber < arrowRow.getChildCount(); arrowViewNumber++) {
                        View arrowCell = arrowRow.getChildAt(arrowViewNumber);
                        Button arrowButton = (Button) arrowCell.findViewById(R.id.arrow_button);
                        arrowButton.setTag(new ArrowInfo(e.getArrows().get(arrowsConfigured)));
                        updateArrowButton(arrowButton);
                        arrowButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                keyboardManager.show();
                                selectedArrow = (Button) view;
                                selectedArrow.setText("?");
                                view.setBackgroundResource(ArcheryToolsApp.getDrawable("green", context.getPackageName()));
                            }
                        });
                        // Set the next button tag for auto advancement
                        if (prevButton != null)
                            ((ArrowInfo) prevButton.getTag()).setNextButton(arrowButton);
                        prevButton = arrowButton;
                        arrowsConfigured++;
                    }
                }
            }
            // Add extra arrow rows if the view needs it
            if (arrowsConfigured < e.getArrows().size()) {
                int rowCounter = 0;
                LinearLayout arrowRow = (LinearLayout) inflater.inflate(R.layout.arrow_row, arrowLayout, false);
                Button prevButton = null;
                for (int arrowNumber = arrowsConfigured; arrowNumber < e.getArrows().size(); arrowNumber++) {
                    if (rowCounter >= 6) {
                        arrowLayout.addView(arrowRow);
                        arrowRow = (LinearLayout) inflater.inflate(R.layout.arrow_row, arrowLayout, false);
                        rowCounter = 0;
                    }

                    View arrowCell = inflater.inflate(R.layout.arrow_cell, arrowLayout, false);
                    Button arrowButton = (Button) arrowCell.findViewById(R.id.arrow_button);
                    arrowButton.setTag(new ArrowInfo(e.getArrows().get(arrowNumber)));
                    updateArrowButton(arrowButton);
                    arrowButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            keyboardManager.show();
                            selectedArrow = (Button) view;
                            selectedArrow.setText("?");
                            view.setBackgroundResource(ArcheryToolsApp.getDrawable("green", context.getPackageName()));
                        }
                    });
                    // Set the next button tag for auto advancement
                    if (prevButton != null)
                        ((ArrowInfo) prevButton.getTag()).setNextButton(arrowButton);
                    prevButton = arrowButton;
                    arrowRow.addView(arrowCell);
                    rowCounter++;
                }
                // If there was a row that was not completely filled after adding all arrows add it.
                if (rowCounter > 0)
                    arrowLayout.addView(arrowRow);
            }
        }

        return reuseView;
    }
}
