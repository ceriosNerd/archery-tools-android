package me.querol.archerytools;

/**
 * Created by winsock on 6/8/14.
 */
public class KeyDOM {
    private String keyText;
    private int keyValue;
    private int keyRank;
    private String keyColorImage;
    private String textColor;

    public KeyDOM(String keyText, int keyValue, int keyRank, String keyColorImage, String textColor) {
        this.keyText = keyText;
        this.keyValue = keyValue;
        this.keyRank = keyRank;
        this.keyColorImage = keyColorImage;
        this.textColor = textColor;
    }

    public String getKeyText() {
        return keyText;
    }

    public int getKeyValue() {
        return keyValue;
    }

    public int getKeyRank() {
        return keyRank;
    }

    public String getKeyColorImage() {
        return keyColorImage;
    }

    public String getTextColor() {
        return textColor;
    }
}
