package me.querol.archerytools;

/**
 * Created by winsock on 6/8/14.
 */
public class SpecialKeyDOM {
    private int code;
    private int width, height;
    private String image;

    public SpecialKeyDOM(int code, int width, int height, String image) {
        this.code = code;
        this.width = width;
        this.height = height;
        this.image = image;
    }

    public int getCode() {
        return code;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getImage() {
        return image;
    }
}
