package me.querol.archerytools;

import java.util.ArrayList;

/**
 * Created by winsock on 5/21/14.
 */
public class EventDOM {
    private ArrayList<RoundDOM> rounds = new ArrayList<RoundDOM>();
    private String name;
    private String keyboardFile;

    public ArrayList<RoundDOM> getRounds() {
        return rounds;
    }

    public String getName() {
        return name;
    }

    public void addRound(RoundDOM round) {
        rounds.add(round);
    }

    public String getKeyboardFile() {
        return keyboardFile;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setKeyboardFile(String keyboardFile) {
        this.keyboardFile = keyboardFile;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
