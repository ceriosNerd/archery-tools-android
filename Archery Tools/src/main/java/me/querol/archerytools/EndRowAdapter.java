package me.querol.archerytools;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import me.querol.archerytools.dao.Arrow;
import me.querol.archerytools.dao.End;
import me.querol.archerytools.dao.ScoreCard;

/**
 * Created by winsock on 5/6/14.
 */
public class EndRowAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private final ScoreCard scoreCard;

    public EndRowAdapter(Context context, ScoreCard scoreCard) {
        // Caches the LayoutInflater for quicker use
        this.inflater = LayoutInflater.from(context);
        this.scoreCard = scoreCard;
    }

    @Override
    public boolean isEnabled(int i) {
        return i < scoreCard.getEvent().getRounds().size();
    }

    @Override
    public int getCount() {
        return scoreCard.getEvent().getRounds().size();
    }

    @Override
    public Object getItem(int i) {
        return scoreCard.getEvent().getRounds().get(i);
    }

    @Override
    public long getItemId(int i) {
        return scoreCard.getEvent().getRounds().get(i).getId();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null)
            view = inflater.inflate(R.layout.round_row, viewGroup, false);
        LinearLayout endCells = (LinearLayout) view.findViewById(R.id.round_cell);
        for (End e : scoreCard.getEvent().getRounds().get(i).getEnds()) {
            RelativeLayout endLayout = (RelativeLayout) inflater.inflate(R.layout.end_row, endCells, false);
            TextView endNumberText = (TextView) endLayout.findViewById(R.id.end_number_text);
            TextView endTotalText = (TextView) endLayout.findViewById(R.id.end_total_text);
            TextView runningTotalText = (TextView) endLayout.findViewById(R.id.running_total_text);
            endNumberText.setText(e.getEndNumber().toString());
            endTotalText.setText(e.getEndTotal().toString());
            runningTotalText.setText(e.getRunningTotal().toString());
            endLayout.setTag(e);

            LinearLayout arrowCells = (LinearLayout) endLayout.findViewById(R.id.arrow_layout);
            for (Arrow a : e.getArrows()) {
                RelativeLayout arrowLayout = (RelativeLayout) inflater.inflate(R.layout.arrow_cell, endLayout, false);
                arrowLayout.setTag(a);
                Button arrowButton = (Button) arrowLayout.findViewById(R.id.arrow_button);
                arrowButton.setText(a.getArrowText());
                arrowButton.setTextColor(Color.parseColor(a.getTextColor()));
                // arrowButton.setBackground(Drawable.createFromPath(a.getArrowImage()));
                arrowCells.addView(arrowLayout);
            }
            endCells.addView(endLayout);
        }
        return view;
    }

    @Override
    public boolean isEmpty() {
        return scoreCard.getEvent().getRounds().size() > 0;
    }
}
