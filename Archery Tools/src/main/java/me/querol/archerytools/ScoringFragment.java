package me.querol.archerytools;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import me.querol.archerytools.dao.ScoreCard;

/**
 * Created by winsock on 5/6/14.
 */
public class ScoringFragment extends Fragment {

    private ScoreCard scoreCard;
    private KeyboardManager keyboardManager;

    public ScoringFragment() {
        if (getArguments() != null && getArguments().containsKey("id")) {
            scoreCard = ArcheryToolsApp.getSessionSingleton().getScoreCardDao().loadDeep(getArguments().getLong("id"));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View scoringView = inflater.inflate(R.layout.fragment_scoring, container, false);
        ListView endListView = (ListView) scoringView.findViewById(R.id.round_list);
        keyboardManager = new KeyboardManager(scoringView, scoreCard.getEvent().getKeyboardFile());
        endListView.setAdapter(new RoundAdapter(container.getContext(), scoreCard, keyboardManager));
        return scoringView;
    }

    public void setScoreCard(ScoreCard scoreCard) {
        this.scoreCard = scoreCard;
        Bundle scoreCardBundle = new Bundle();
        scoreCardBundle.putLong("id", scoreCard.getId());
        this.setArguments(scoreCardBundle);
    }
}