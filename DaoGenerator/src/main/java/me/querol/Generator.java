package me.querol;

import java.io.IOException;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class Generator {
    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(1, "me.querol.archerytools.dao");
        schema.enableActiveEntitiesByDefault();

        /*
        Score Card
         */
        Entity scoreCard = schema.addEntity("ScoreCard");
        scoreCard.addIdProperty().primaryKey();
        scoreCard.addStringProperty("archerName");
        scoreCard.addStringProperty("eventName");
        scoreCard.addDateProperty("eventDate");

        /*
        Event
         */
        Entity event = schema.addEntity("Event");
        event.addIdProperty().primaryKey();
        event.addStringProperty("keyboardFile");
        event.addStringProperty("typeName");
        Property eventRelation = scoreCard.addLongProperty("eventId").notNull().getProperty();
        scoreCard.addToOne(event, eventRelation, "event");
        Property eventParent = event.addLongProperty("parentId").notNull().getProperty();
        event.addToOne(scoreCard, eventParent, "parent");

        /*
        Round
         */
        Entity round = schema.addEntity("Round");
        round.addIdProperty().primaryKey();
        round.addIntProperty("roundNumber");
        round.addStringProperty("distanceText");
        round.addIntProperty("arrowsPerEnd");
        round.addIntProperty("endsPerRound");
        round.addIntProperty("hits");
        round.addIntProperty("totalShot");
        round.addFloatProperty("avg");
        round.addIntProperty("total");
        Property toManyRounds = event.addLongProperty("roundsId").getProperty();
        event.addToMany(round, toManyRounds, "rounds");
        Property roundParent = round.addLongProperty("parentId").notNull().getProperty();
        round.addToOne(event, roundParent, "parent");

        /*
        End
         */
        Entity end = schema.addEntity("End");
        end.addIdProperty().primaryKey();
        end.addIntProperty("endNumber");
        end.addIntProperty("hits");
        end.addIntProperty("shots");
        end.addIntProperty("endTotal");
        end.addIntProperty("runningTotal");
        end.addByteArrayProperty("image");
        Property toManyEnds = round.addLongProperty("endsId").getProperty();
        round.addToMany(end, toManyEnds, "ends");
        Property endParent = end.addLongProperty("parentId").notNull().getProperty();
        end.addToOne(round, endParent, "parent");

        /*
        Arrow
         */
        Entity arrow = schema.addEntity("Arrow");
        arrow.addIdProperty().primaryKey();
        arrow.addIntProperty("arrowNumber");
        arrow.addStringProperty("arrowImage");
        arrow.addStringProperty("arrowText");
        arrow.addStringProperty("textColor");
        arrow.addIntProperty("arrowValue");
        arrow.addIntProperty("arrowOrder");
        Property toManyArrows = end.addLongProperty("arrowsId").getProperty();
        end.addToMany(arrow, toManyArrows, "arrows");
        Property arrowParent = arrow.addLongProperty("parentId").notNull().getProperty();
        arrow.addToOne(end, arrowParent, "parent");

        new DaoGenerator().generateAll(schema, "Archery Tools/src-gen");
    }
}
